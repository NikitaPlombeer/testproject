package com.plombeer.handler.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * Created by plombeer on 02.12.16.
 */
@Configuration
@ComponentScan(basePackages = "com.plombeer.handler")
@ImportResource(value = {"classpath:applicationContext.xml"})
public class AppConfig {

//    @Bean
//    public ActiveMQConnectionFactory amqConnectionFactory() {
//        ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory("tcp://localhost:61616");
//        factory.setTrustAllPackages(true);
//        return factory;
//    }
//
//    @Bean
//    public DestinationResolver destinationResolver() {
//        return new BeanFactoryDestinationResolver();
//    }
//
//    @Bean
//    public DefaultJmsListenerContainerFactory jmsListenerContainerFactory() {
//        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
//        factory.setConnectionFactory(amqConnectionFactory());
//        factory.setDestinationResolver(destinationResolver());
//        return factory;
//    }
//
//    @Bean
//    public Destination first(){
//        return new ActiveMQQueue("first");
//    }
}
