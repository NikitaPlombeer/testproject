package com.plombeer.handler;

import com.plombeer.entity.Task;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class Receiver {

    @JmsListener(destination = "first")
    public void receiveMessage(Task task) {
        System.out.println("Received <" + task + ">");
    }

}