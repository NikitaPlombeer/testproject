package com.plombeer.test.controller;

import com.plombeer.entity.Task;
import com.plombeer.test.service.SenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by plombeer on 01.12.16.
 */
@RestController
public class TaskController {

    private SenderService service;

    @RequestMapping(value = "/task", method = RequestMethod.POST)
    public Task getHello(@ModelAttribute Task task){
        System.out.println(task);
        service.send(task);
        return task;
    }

    @Autowired
    public void setService(SenderService service) {
        this.service = service;
    }
}
