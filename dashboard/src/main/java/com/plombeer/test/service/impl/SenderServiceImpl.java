package com.plombeer.test.service.impl;

import com.plombeer.entity.Task;
import com.plombeer.test.service.SenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;


/**
 * Created by plombeer on 01.12.16.
 */
@Service
public class SenderServiceImpl implements SenderService {

    private JmsTemplate jmsTemplate;

    public void send(Task task) {
        jmsTemplate.convertAndSend(task);
    }

    @Autowired
    public void setJmsTemplate(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }
}
