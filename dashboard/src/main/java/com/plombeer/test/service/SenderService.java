package com.plombeer.test.service;


import com.plombeer.entity.Task;

/**
 * Created by plombeer on 01.12.16.
 */
public interface SenderService {

    void send(Task task);
}
